<?php

declare(strict_types=1);

namespace Drupal\Tests\ffmpeg_image_toolkit\Kernel;

use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\FfmpegToolkit;
use Drupal\KernelTests\KernelTestBase;
use Drupal\system\SystemManager;

/**
 * Tests ffmpeg image toolkit operations.
 *
 * Those tests will fail if ffmpeg is not installed on the system.
 *
 * @requires executable ffmpeg
 * @group ffmpeg_image_toolkit
 */
final class FfmpegToolkitTest extends KernelTestBase {

  /**
   * Array of images used for testing.
   *
   * @var string[]
   */
  private array $images;

  /**
   * The core image toolkit manager.
   */
  private ImageToolkitManager $imageToolkitManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ffmpeg_image_toolkit',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->images = [
      // Standard PNG.
      'png' => \DRUPAL_ROOT . '/core/misc/druplicon.png',
      // JPEG.
      'jpeg' => __DIR__ . '/../../sample/elephant.jpeg',
      // GIF.
      'gif' => __DIR__ . '/../../sample/elephant.gif',
      // Animated png - APNG.
      'apng' => __DIR__ . '/../../sample/elephant.png',
    ];

    $this->imageToolkitManager = $this->container->get('image.toolkit.manager');
  }

  /**
   * Data provider method.
   *
   * We don't need to reinstall for every case so this method will be called
   * by the test method.
   *
   * @return mixed[]
   *   Test operation data.
   */
  private function getOperations(): array {
    return [
      'scale' => [
        'width' => 25,
        'height' => 25,
      ],
      'scale_and_crop' => [
        'x' => 25,
        'y' => 10,
        'width' => 50,
        'height' => 40,
      ],
      'convert' => [
        'extension' => 'jpeg',
      ],
      'rotate' => [
        'parameters' => ['degrees' => 90],
        'expected' => [
          'width' => 80,
          'height' => 100,
        ],
      ],
    ];
  }

  /**
   * The actual test.
   */
  public function testFfmpegToolkit(): void {
    // Check requirements.
    $toolkit = $this->imageToolkitManager->createInstance('ffmpeg');
    $requirements = $toolkit->getRequirements();
    $this->assertArrayHasKey('version', $requirements, 'Requirements not properly built.');
    $this->assertEquals(SystemManager::REQUIREMENT_OK, $requirements['version']['severity'], 'Ffmpeg is not installed.');

    foreach ($this->images as $format => $image_path) {
      $toolkit = $this->imageToolkitManager->createInstance('ffmpeg');
      \assert($toolkit instanceof FfmpegToolkit);

      $toolkit->setSource($image_path);
      $parse_result = $toolkit->parseFile();
      $this->assertTrue($parse_result);
      $this->assertEquals($format, $toolkit->getFormat());

      // Png is an image provided by Drupal, input width and height are
      // subject to change.
      if ($format === 'png') {
        continue;
      }

      $this->assertEquals(100, $toolkit->getWidth());
      $this->assertEquals(80, $toolkit->getHeight());

      // Apply some operations..
      // We don't need extension on the output file, ffmpeg parses contents
      // anyway. Use temporary stream wrapper as vfs streams don't resolve
      // to a real path that's needed by ffmpeg easily.
      $destination = 'temporary://ffmpeg_test_output';
      // We need to instantiate another ffmpeg toolkit for verifications
      // - source can be set only once.
      $verifying_toolkit = $this->imageToolkitManager->createInstance('ffmpeg');
      $verifying_toolkit->setSource($destination);
      foreach ($this->getOperations() as $operation => $data) {
        if (!\array_key_exists('parameters', $data)) {
          $data['parameters'] = $data;
        }
        if (!\array_key_exists('expected', $data)) {
          $data['expected'] = $data;
        }

        $toolkit->apply($operation, $data['parameters']);
        $result = $toolkit->save($destination);
        $this->assertTrue($result);

        $verifying_toolkit->parseFile();

        if (\array_key_exists('width', $data['expected'])) {
          $this->assertEquals($data['expected']['width'], $verifying_toolkit->getWidth(), \sprintf('Image width expected %d, current %d, operation %s, source %s', $data['expected']['width'], $verifying_toolkit->getWidth(), $operation, $image_path));
        }
        if (\array_key_exists('height', $data['expected'])) {
          $this->assertEquals($data['expected']['height'], $verifying_toolkit->getHeight(), \sprintf('Image height expected %d, current %d, operation %s, source %s', $data['expected']['height'], $verifying_toolkit->getHeight(), $operation, $image_path));
        }
        if (\array_key_exists('extension', $data['expected'])) {
          $this->assertEquals($data['expected']['extension'], $verifying_toolkit->getFormat(), \sprintf('Image format expected %s, current %s, operation %s, source %s', $data['expected']['extension'], $verifying_toolkit->getFormat(), $operation, $image_path));
        }

        \unlink($destination);
      }
    }
  }

}
