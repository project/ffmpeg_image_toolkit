<?php

declare(strict_types=1);

namespace Drupal\ffmpeg_image_toolkit;

use Drupal\ffmpeg_image_toolkit\Exception\FfmpegException;

/**
 * Provides a service for executing ffmpeg commands on server.
 */
final class Ffmpeg {

  /**
   * Execute a ffmpeg command.
   *
   * @return string[]
   *   Output from PHP exec() function 2nd argument - array of stdout lines.
   */
  public function execute(array $parameters, string $format = ''): array {
    $command = 'ffmpeg -hide_banner -y';

    if (\array_key_exists('input', $parameters) && $parameters['input'] !== NULL) {
      $input_path = $parameters['input'];
      $parameters['input'] = '-i "' . $input_path . '"';
    }

    foreach ([
      'input',
      'output_parameters',
      'output_file',
    ] as $parameter_group) {
      if (
        !\array_key_exists($parameter_group, $parameters) ||
        $parameters[$parameter_group] === NULL
      ) {
        continue;
      }
      $command .= ' ' . $parameters[$parameter_group];
    }

    // Just in case disallow certain characters in the command.
    foreach ([';', '|', '>', '&'] as $disallowed_character) {
      if (\strpos($command, $disallowed_character) !== FALSE) {
        throw new FfmpegException(\sprintf('Invalid command: %s; disallowed characters found.', $command));
      }
    }

    // If no output file is specified, we'll not get a success return code
    // but we need debug information about the file that's normally in stderr.
    $command .= ' 2>&1';

    $result_code = 0;
    $output = [];
    \exec($command, $output, $result_code);

    // Check if this is a get info only operation and update result code
    // accordingly.
    $output_count = \count($output);
    if (
      $result_code !== 0 &&
      $parameters['output_file'] === NULL &&
      $output_count !== 0
    ) {
      for ($i = $output_count - 1; $i > 0; $i--) {
        if (\strpos($output[$i], 'Successfully opened the file') !== FALSE) {
          $result_code = 0;
        }
      }
    }

    if ($result_code !== 0) {
      // Pass the last line from the output, hopefully something useful
      // is there (no such file or directory info for example).
      throw new FfmpegException(\sprintf('Invalid command: %s; last output line: %s', $command, \array_pop($output)));
    }

    return $output;
  }

}
