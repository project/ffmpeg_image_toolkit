<?php

namespace Drupal\ffmpeg_image_toolkit\Exception;

/**
 * Defines ffmpeg exception.
 */
final class FfmpegException extends \Exception {

}
