<?php

declare(strict_types=1);

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ImageToolkit\ImageToolkitBase;
use Drupal\Core\ImageToolkit\ImageToolkitOperationManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\ffmpeg_image_toolkit\Exception\FfmpegException;
use Drupal\ffmpeg_image_toolkit\Ffmpeg;
use Drupal\system\SystemManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Ffmpeg toolkit with animation support.
 *
 * @ImageToolkit(
 *   id = "ffmpeg",
 *   title = @Translation("Ffmpeg toolkit with animation support")
 * )
 */
final class FfmpegToolkit extends ImageToolkitBase {

  private const SUPPORTED_TYPES = [
    'png' => \IMAGETYPE_PNG,
    'apng' => \IMAGETYPE_PNG,
    'gif' => \IMAGETYPE_GIF,
    'jpeg' => \IMAGETYPE_JPEG,
    'webp' => \IMAGETYPE_WEBP,
  ];

  private const FORMAT_MAP = [
    'mjpeg' => 'jpeg',
    'mjpe' => 'jpeg',
  ];

  private const ANIMATION_FORMATS = ['apng', 'gif'];

  /**
   * Media format.
   */
  private string $format = '';

  /**
   * Media width.
   */
  private int $width = 0;

  /**
   * Media height.
   */
  private int $height = 0;

  /**
   * APNG only - number of plays.
   */
  private int $plays = 0;

  /**
   * Temporary file for output.
   */
  private string $outTmpFilePath = '';

  /**
   * Array of filters to apply to the media file.
   */
  private ?array $filters = NULL;

  /**
   * Constructs a FfmpegToolkit object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ImageToolkitOperationManagerInterface $operation_manager,
    LoggerInterface $logger,
    ConfigFactoryInterface $config_factory,
    private StreamWrapperManagerInterface $streamWrapperManager,
    private FileSystemInterface $fileSystem,
    private Ffmpeg $ffmpeg
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $operation_manager, $logger, $config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('image.toolkit.operation.manager'),
      $container->get('logger.channel.image'),
      $container->get('config.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('ffmpeg')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['jpeg_quality'] = [
      '#type' => 'number',
      '#title' => $this->t('JPEG quality'),
      '#description' => $this->t('Define the image quality for JPEG manipulations. Ranges from 0 to 31. Lower values mean better image quality but bigger files.'),
      '#min' => 0,
      '#max' => 31,
      '#default_value' => $this->configFactory->get('ffmpeg_image_toolkit.config')->get('jpeg_quality', 10),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('ffmpeg_image_toolkit.config')
      ->set('jpeg_quality', $form_state->getValue(['ffmpeg', 'jpeg_quality']))
      ->save();
  }

  /**
   * Get absolute server path of the media file.
   */
  private function getRealpath(?string $source = NULL): string {
    if ($source === NULL) {
      $source = $this->getSource();
    }

    $result = $this->fileSystem->realpath($source);
    if ($result !== FALSE) {
      return $result;
    }

    // No stream wrapper case.
    if ($source[0] === '/') {
      return $source;
    }
    return \DRUPAL_ROOT . '/' . $source;
  }

  /**
   * Execute a ffmpeg command.
   *
   * Needed in operation plugins so we don't have to explicitly inject
   * the service everywhere.
   *
   * @see \Drupal\ffmpeg_image_toolkit\Ffmpeg::execute()
   */
  public function ffmpegExecute(array $parameters): array {
    if (!\array_key_exists('input', $parameters)) {
      if ($this->outTmpFilePath === '') {
        throw new \Exception('Ffmpeg operation executed before initialization.');
      }
      $parameters['input'] = $this->getRealpath($this->getRealpath());
    }
    if (!\array_key_exists('output_file', $parameters)) {
      $parameters['output_file'] = $this->getRealpath($this->outTmpFilePath);

      // Add plays parameter if this is a apng (ffmpeg default is 1 and not
      // the source value for some reason).
      if ($this->format === 'apng') {
        $parameters['output_parameters'] .= \sprintf(' -plays %d', $this->plays);
      }

      // Add quality for jpg.
      if ($this->format === 'jpeg') {
        $parameters['output_parameters'] .= \sprintf(' -q:v %d', $this->configFactory->get('ffmpeg_image_toolkit.config')->get('jpeg_quality', 10));
      }

    }

    return $this->ffmpeg->execute($parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function parseFile() {
    $input_path = $this->getRealpath();
    if ($input_path === '') {
      return FALSE;
    }

    // To parse a file we have to load it to ffmpeg with debug enabled,
    // so far that's the only way to get apng loop setting.
    try {
      $result = $this->ffmpegExecute([
        'input' => $input_path,
        'output_parameters' => '-v debug',
        'output_file' => NULL,
      ]);
    }
    catch (FfmpegException $e) {
      $this->logger->error("Ffmpeg toolkit can not parse file '@file'. @error", [
        '@file' => $this->getSource(),
        '@error' => $e->getMessage(),
      ]);
      return FALSE;
    }

    $stream_info_string = '';
    $nrows = \count($result);
    for ($i = $nrows - 1; $i > 0; $i--) {
      if (\strpos($result[$i], 'Stream #0:0')) {
        $stream_info_string = $result[$i];
      }
    }

    if ($stream_info_string === '') {
      return FALSE;
    }

    $matches = [];
    $match_result = \preg_match('/Video: ([a-z]{3,4}).*?([0-9]+)x([0-9]+)/', $stream_info_string, $matches);
    if ($match_result !== 1) {
      return FALSE;
    }

    $this->format = \array_key_exists($matches[1], self::FORMAT_MAP) ? self::FORMAT_MAP[$matches[1]] : $matches[1];
    $this->width = (int) $matches[2];
    $this->height = (int) $matches[3];

    // Get number of plays if this is an APNG.
    if ($this->format === 'apng') {
      for ($i = 0; $i < $nrows; $i++) {
        if (\strpos($result[$i], 'num_play:') !== FALSE) {
          $match_result = \preg_match('/num_play: ([0-9]+)/', $result[$i], $matches);
          if ($match_result === 1) {
            $this->plays = (int) $matches[1];
          }
          break;
        }
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    if (!\array_key_exists($this->format, self::SUPPORTED_TYPES)) {
      return FALSE;
    }
    return $this->width > 0 &&
      $this->height > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight() {
    return $this->height;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth() {
    return $this->width;
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType() {
    if (!\array_key_exists($this->format, self::SUPPORTED_TYPES)) {
      return '';
    }

    return \image_type_to_mime_type(self::SUPPORTED_TYPES[$this->format]);
  }

  /**
   * Get ffmpeg format.
   */
  public function getFormat(): string {
    return $this->format;
  }

  /**
   * Helper function that initializes the temp file.
   */
  private function initTempFile(): void {
    $out_path = $this->fileSystem->tempnam('temporary://', 'ffmpeg_');

    // Drupal file system tempnam doesn't support suffixes (extensions) and
    // ffmpeg relies on those while determining output formats so..
    $this->outTmpFilePath = $out_path . '.' . $this->format;
    $this->fileSystem->move($out_path, $this->outTmpFilePath, FileSystemInterface::EXISTS_REPLACE);

    // Copy source in case of no-operation save.
    $this->fileSystem->copy($this->getSource(), $this->outTmpFilePath, FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * {@inheritdoc}
   */
  public function save($destination) {
    try {
      $this->initTempFile();
    }
    catch (FileException $e) {
      $this->logger->error("Unable to create a temporary file for ffmpeg. @details", [
        '@details' => $e->getMessage(),
      ]);
      return FALSE;
    }

    try {
      $this->applyFilters();
    }
    catch (FfmpegException $e) {
      $this->logger->error("Unable to apply ffmpeg filters. @details", [
        '@details' => $e->getMessage(),
      ]);
      return FALSE;
    }

    try {
      $this->fileSystem->move($this->outTmpFilePath, $destination, FileSystemInterface::EXISTS_REPLACE);
    }
    catch (FileException $e) {
      $this->logger->error("Unable to move output file to destination. @details", [
        '@details' => $e->getMessage(),
      ]);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequirements() {
    $requirements = [];

    $requirements['version'] = [
      'title' => $this->t('Ffmpeg'),
      'severity' => SystemManager::REQUIREMENT_OK,
      'value' => '',
    ];
    try {
      $version_output = $this->ffmpeg->execute([
        'output_parameters' => '-version',
        'output_file' => NULL,
      ]);
    }
    catch (FfmpegException $e) {
      $version_output = [];
    }

    $match_result = 0;
    foreach ($version_output as $line) {
      $matches = [];
      $match_result = \preg_match('/ffmpeg version ([^ ]+)/', $line, $matches);
      if ($match_result === 1) {
        $requirements['version']['value'] = $matches[1];
        break;
      }
    }

    if ($match_result !== 1) {
      $requirements['version']['severity'] = SystemManager::REQUIREMENT_ERROR;
      $requirements['version']['description'] = $this->t('Ffmpeg library is not installed.');
    }
    return $requirements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isAvailable() {
    try {
      \Drupal::service('ffmpeg')->execute([
        'output_parameters' => '-version',
        'output_file' => NULL,
      ]);
    }
    catch (FfmpegException $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSupportedExtensions() {
    $supported = [];
    foreach ([
      \IMAGETYPE_PNG,
      \IMAGETYPE_JPEG,
      \IMAGETYPE_GIF,
      \IMAGETYPE_WEBP,
    ] as $image_type) {
      $supported[] = \mb_strtolower(\image_type_to_extension($image_type, FALSE));
    }
    $supported[] = 'apng';
    $supported[] = 'jpg';

    return $supported;
  }

  /**
   * Add a filter.
   */
  public function addFilter(string $filter, array $parameters = []): void {
    if (\count($parameters) === 0) {
      $this->filters[] = $filter;
    }
    else {
      $this->filters[] = \vsprintf($filter, $parameters);
    }
  }

  /**
   * Apply filters on the input file and save to output.
   */
  private function applyFilters(): void {
    if ($this->filters === NULL) {
      return;
    }

    // Add first frame only processing if output is not an animation format.
    // Yes, it's fine if input is a single image as well.
    if (!\in_array($this->format, self::ANIMATION_FORMATS)) {
      $this->filters[] = 'select=eq(n\,0)';
    }

    // Empty filters - format conversion only.
    if (\count($this->filters) === 0) {
      $this->ffmpegExecute([]);
    }
    // Standard operation.
    else {
      $this->ffmpegExecute([
        'output_parameters' => '-vf "' . \implode(',', $this->filters) . '"',
      ]);
    }

    $this->filters = NULL;
  }

  /**
   * Set a different file format.
   */
  public function setFormat(string $format): void {
    if ($format === $this->format) {
      return;
    }

    $this->format = $format;

    // We need to execute ffmpeg conversion even if filters are empty.
    if ($this->filters === NULL) {
      $this->filters = [];
    }
  }

}
