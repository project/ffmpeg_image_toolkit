<?php

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\Operation\ffmpeg;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Resize as GdResize;

/**
 * Defines ffmpeg resize operation.
 *
 * @ImageToolkitOperation(
 *   id = "ffmpeg_resize",
 *   toolkit = "ffmpeg",
 *   operation = "resize",
 *   label = @Translation("Resize"),
 *   description = @Translation("Resizes an image to the given dimensions (ignoring aspect ratio).")
 * )
 */
class Resize extends GdResize {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []) {
    $this->toolkit->addFilter('scale=%d:%d', [
      $arguments['width'],
      $arguments['height'],
    ]);

    return TRUE;
  }

}
