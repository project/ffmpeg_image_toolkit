<?php

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\Operation\ffmpeg;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Scale as GdScale;

/**
 * Defines ffmpeg scale operation.
 *
 * @ImageToolkitOperation(
 *   id = "ffmpeg_scale",
 *   toolkit = "ffmpeg",
 *   operation = "scale",
 *   label = @Translation("Scale"),
 *   description = @Translation("Scales an image while maintaining aspect ratio. The resulting image can be smaller for one or both target dimensions.")
 * )
 */
class Scale extends GdScale {

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments) {
    // Assure at least one dimension.
    if (empty($arguments['width']) && empty($arguments['height'])) {
      throw new \InvalidArgumentException("At least one dimension ('width' or 'height') must be provided to the image 'scale' operation");
    }

    foreach (['width', 'height'] as $argument) {
      if (!empty($arguments[$argument])) {
        $arguments[$argument] = (int) \round($arguments[$argument]);
        if ($arguments[$argument] <= 0) {
          throw new \InvalidArgumentException("Invalid $argument ('{$arguments[$argument]}') specified for the image 'resize' operation");
        }
      }
      else {
        $arguments[$argument] = -1;
      }
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []) {
    // Don't scale if we don't change the dimensions at all.
    if ($arguments['width'] === $this->getToolkit()->getWidth() && $arguments['height'] === $this->getToolkit()->getHeight()) {
      return TRUE;
    }

    // Don't upscale if the option isn't enabled.
    if (
      !$arguments['upscale'] && (
        $arguments['width'] >= $this->getToolkit()->getWidth() ||
        $arguments['height'] >= $this->getToolkit()->getHeight()
      )
    ) {
      return TRUE;
    }

    $this->toolkit->addFilter('scale=%d:%d', [
      $arguments['width'],
      $arguments['height'],
    ]);

    return TRUE;
  }

}
