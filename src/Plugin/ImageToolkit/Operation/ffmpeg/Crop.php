<?php

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\Operation\ffmpeg;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Crop as GdCrop;

/**
 * Defines ffmpeg crop operation.
 *
 * @ImageToolkitOperation(
 *   id = "ffmpeg_crop",
 *   toolkit = "ffmpeg",
 *   operation = "crop",
 *   label = @Translation("Crop"),
 *   description = @Translation("Crops an image to a rectangle specified by the given dimensions.")
 * )
 */
class Crop extends GdCrop {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []) {
    $this->toolkit->addFilter('crop=%d:%d:%d:%d', [
      $arguments['width'],
      $arguments['height'],
      $arguments['x'],
      $arguments['y'],
    ]);

    return TRUE;
  }

}
