<?php

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\Operation\ffmpeg;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\ScaleAndCrop as GdScaleAndCrop;

/**
 * Defines ffmpeg scale and crop operation.
 *
 * @ImageToolkitOperation(
 *   id = "ffmpeg_scale_and_crop",
 *   toolkit = "ffmpeg",
 *   operation = "scale_and_crop",
 *   label = @Translation("Scale and crop"),
 *   description = @Translation("Scales an image to the exact width and height given. This plugin achieves the target aspect ratio by cropping the original image equally on both sides, or equally on the top and bottom. This function is useful to create uniform sized avatars from larger images.")
 * )
 */
class ScaleAndCrop extends GdScaleAndCrop {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []) {
    $this->toolkit->addFilter('scale=%d:%d', [
      $arguments['resize']['width'],
      $arguments['resize']['height'],
    ]);
    $this->toolkit->addFilter('crop=%d:%d:%d:%d', [
      $arguments['width'],
      $arguments['height'],
      $arguments['x'],
      $arguments['y'],
    ]);

    return TRUE;
  }

}
