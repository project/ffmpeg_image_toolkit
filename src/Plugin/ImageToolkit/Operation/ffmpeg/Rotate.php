<?php

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\Operation\ffmpeg;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Rotate as GdRotate;

/**
 * Defines ffmpeg rotate operation.
 *
 * @ImageToolkitOperation(
 *   id = "ffmpeg_rotate",
 *   toolkit = "ffmpeg",
 *   operation = "rotate",
 *   label = @Translation("Rotate"),
 *   description = @Translation("Rotates an image by the given number of degrees.")
 * )
 */
class Rotate extends GdRotate {

  /**
   * {@inheritdoc}
   */
  protected function validateArguments(array $arguments) {
    $arguments['degrees'] -= \floor($arguments['degrees'] / 360) * 360;

    // Validate color string so we don't have surprises.
    $color_valid = TRUE;
    if ($arguments['background'] !== NULL && $arguments['background'] !== '') {
      if (!\str_starts_with($arguments['background'], '#')) {
        $color_valid = FALSE;
      }
      else {
        $hash = \substr($arguments['background'], 1);
        $length = \strlen($hash);

        if (!\ctype_xdigit($hash)) {
          $color_valid = FALSE;
        }
        if ($length !== 6) {
          throw new \InvalidArgumentException("Invalid color provided, ffmpeg will work only with 6-character long string after the #.");
        }
      }
    }

    if (!$color_valid) {
      throw new \InvalidArgumentException("Invalid color provided.");
    }

    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []) {
    // Transpose operation with no background required actually.
    if ($arguments['degrees'] % 90 === 0) {
      $arguments['degrees'] = (int) $arguments['degrees'];
      if ($arguments['degrees'] === 90) {
        $this->toolkit->addFilter('transpose=1');
      }
      elseif ($arguments['degrees'] === 180) {
        $this->toolkit->addFilter('transpose=1');
        $this->toolkit->addFilter('transpose=1');
      }
      elseif ($arguments['degrees'] === 270) {
        $this->toolkit->addFilter('transpose=2');
      }
    }
    else {
      $filter = 'rotate=%d*(PI/180):c=%s';
      $filter_arguments = [$arguments['degrees']];
      if (!empty($arguments['background'])) {
        $filter_arguments[] = $arguments['background'];
      }
      else {
        // @todo This results in black for most formats; works fine on apng.
        $filter_arguments[] = 'none';
      }
      $this->toolkit->addFilter($filter, $filter_arguments);
    }

    return TRUE;
  }

}
