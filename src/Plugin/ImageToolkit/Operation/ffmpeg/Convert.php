<?php

namespace Drupal\ffmpeg_image_toolkit\Plugin\ImageToolkit\Operation\ffmpeg;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Convert as GdConvert;

/**
 * Defines ffmpeg convert operation.
 *
 * @ImageToolkitOperation(
 *   id = "ffmpeg_convert",
 *   toolkit = "ffmpeg",
 *   operation = "convert",
 *   label = @Translation("Convert"),
 *   description = @Translation("Instructs the toolkit to save the image with a specified format and extension.")
 * )
 */
class Convert extends GdConvert {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = []) {
    $this->toolkit->setFormat($arguments['extension']);

    return TRUE;
  }

}
