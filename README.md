## INTRODUCTION

Ffmpeg Image Toolkit is an image conversion module that uses ffmpeg framework
(https://ffmpeg.org/) to apply different operations on images. The main
difference between its toolkit and the core toolkit and other toolkits
at the time of writing this is that it can handle all animated image formats
including GIFs and APNGs (animated PNG) apart from handling standard images.


## REQUIREMENTS

The ffmpeg toolkit requires ffmpeg framework installed on the system and
ffmpeg binary added to $PATH so the `ffmpeg` command is available from
any location on the machine (Drupal root in particular).


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.
 * Go to Administration > Configuration > Media > Image toolkit, set
   "Ffmpeg toolkit with animation support" as your image processing toolkit,
   save.
